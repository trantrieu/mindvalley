package trieu.mindvalley_trieu_android_test.view.activities;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import trieu.mindvalley_trieu_android_test.view.fragments.BaseFragment;

/**
 * Created by Apple on 10/1/16.
 */
public class BaseActivity extends AppCompatActivity {

    static private final String REPLACE = "replace ";
    public void replaceFragment(int containerViewId, BaseFragment fragment) {
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(containerViewId, fragment, fragment.getTagName());
        fragmentTransaction.addToBackStack(REPLACE + fragment.getTagName());
        fragmentTransaction.commit();
    }

    public void removeThisFragment(BaseFragment fragment){
        getSupportFragmentManager().popBackStack();
    }

    public void backToFragment(String name){
        getSupportFragmentManager().popBackStack(REPLACE + name, 0);
    }

    public void backToFragmentInclusive(String name){
        getSupportFragmentManager().popBackStack(REPLACE + name, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void popBackStack(){
        getSupportFragmentManager().popBackStack();
    }

    public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

}

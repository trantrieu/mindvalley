package trieu.mindvalley_trieu_android_test.view.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by Apple on 10/1/16.
 */
public abstract class BaseFragment extends Fragment {

    private String tagName;

    protected BaseFragment(){
        tagName = setTagName();
    }
    protected abstract String setTagName();
    public String getTagName(){
        return tagName;
    }
}

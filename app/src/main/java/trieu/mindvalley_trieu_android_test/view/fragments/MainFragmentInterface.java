package trieu.mindvalley_trieu_android_test.view.fragments;

import android.view.View;

import java.util.List;

import trieu.mindvalley_trieu_android_test.model.MyObj;

/**
 * Created by Apple on 10/1/16.
 */
public interface MainFragmentInterface extends FragmentInterface {

    void showContent(List<MyObj> list);
    void showError(String error, Throwable t, View.OnClickListener onClickListener);
    void showLoading();
}

package trieu.mindvalley_trieu_android_test.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import trieu.mindvalley_trieu_android_test.R;
import trieu.mindvalley_trieu_android_test.controller.MainController;
import trieu.mindvalley_trieu_android_test.model.MyObj;
import trieu.mindvalley_trieu_android_test.view.adapters.ObjAdapter;
import trieu.mindvalley_trieu_android_test.view.custom.InfoView;

/**
 * Created by Apple on 10/1/16.
 */
public class MainFragment extends BaseFragment implements MainFragmentInterface{
    private InfoView infoView;
    private ListView lv;
    private SwipeRefreshLayout swipeRefreshLayout;
    private MainController mainController;
    static public MainFragment getInstance(){
        MainFragment mainFragment = new MainFragment();
        return mainFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mainController = new MainController();
        mainController.attactTo(this);

        View view = inflater.inflate(R.layout.fragment_main, null);

        lv = (ListView)view.findViewById(R.id.fragment_main_lv);
        infoView = (InfoView) view.findViewById(R.id.fragment_main_info);
        lv.setEmptyView(infoView);
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.fragment_main_swipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mainController.clearMemAndRequest();
            }
        });
        mainController.request();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mainController.detact();
        mainController = null;
    }

    @Override
    protected String setTagName() {
        return MainFragment.class.getSimpleName();
    }

    @Override
    public void showContent(List<MyObj> list) {
        ObjAdapter imageAdapter = new ObjAdapter(getActivity(), list);
        lv.setAdapter(imageAdapter);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(String error, Throwable t, View.OnClickListener onClickListener) {
        lv.setAdapter(null);
        infoView.showError(t.getLocalizedMessage(), t, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainController.request();
            }
        });
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showLoading() {
        infoView.showLoading();
    }

    @Override
    public Context getCContext() {
        return getActivity();
    }
}

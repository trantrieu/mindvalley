package trieu.mindvalley_trieu_android_test.view.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import trieu.mindvalley_trieu_android_test.R;
import trieu.mindvalley_trieu_android_test.lib.DownloadLibs;
import trieu.mindvalley_trieu_android_test.model.MyObj;

/**
 * Created by Apple on 10/1/16.
 */
public class ObjAdapter extends BaseAdapter {

    private List<MyObj> list;
    private Activity activity;
    public ObjAdapter(Activity activity, List<MyObj> list){
        this.list = list;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        if(convertView == null){
            convertView = inflater.inflate(R.layout.lv_item_layout, null);
        }
        MyObj myObj = list.get(position);
        String url = myObj.getUser().getProfileImage().getLarge();
        ImageView imageView = (ImageView) convertView.findViewById(R.id.lv_item_layout_img);
        DownloadLibs.getImageByUrl(activity, url, null, imageView);

        TextView nameTv = (TextView)convertView.findViewById(R.id.lv_item_layout_name);
        nameTv.setText(myObj.getUser().getName());

        return convertView;
    }

}

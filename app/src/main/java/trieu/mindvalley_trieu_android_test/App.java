package trieu.mindvalley_trieu_android_test;

import android.app.Application;

/**
 * Created by Apple on 10/1/16.
 */
public class App extends Application {
    static public App application;
    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}

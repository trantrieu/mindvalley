package trieu.mindvalley_trieu_android_test.lib;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.Response;

/**
 * Created by Apple on 9/28/16.
 */
class DownloadLibImage extends DownloadLibAbs<Bitmap> {
    public DownloadLibImage(String url) {
        super(url);
    }

    @Override
    public Bitmap connectAndParseObj() throws IOException {
        Response response = connectTo(url);
        InputStream is = response.body().byteStream();
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(is);
        }catch (OutOfMemoryError e){
            e.printStackTrace();
        }
        if(bitmap == null){
            CacheLib.getInstance().clearAll();
            bitmap = BitmapFactory.decodeStream(is);
        }
        return bitmap;
    }
}

package trieu.mindvalley_trieu_android_test.lib;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.Response;

/**
 * Created by Apple on 9/28/16.
 */
class DownloadLibJson extends DownloadLibAbs<String> {
    public DownloadLibJson(String url) {
        super(url);
    }


    @Override
    public String connectAndParseObj() throws IOException, JSONException {
        Response response = connectTo(url);
        String responseStr = response.body().string();
        return  responseStr;
//        JSONObject jsonObject = new JSONObject(responseStr);
//        return jsonObject;
    }
}

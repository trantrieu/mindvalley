package trieu.mindvalley_trieu_android_test.lib;

import org.json.JSONException;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Apple on 9/28/16.
 */
abstract class DownloadLibAbs<T>  {

    protected String url;
    public DownloadLibAbs(String url){
        this.url = url;
    }

    protected Response connectTo(String url) throws IOException {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);
        OkHttpClient client = builder.build();

        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        return response;
    }

    public abstract T connectAndParseObj() throws IOException, JSONException;
}

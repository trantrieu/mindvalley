package trieu.mindvalley_trieu_android_test.controller;

import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import trieu.mindvalley_trieu_android_test.lib.CacheLib;
import trieu.mindvalley_trieu_android_test.lib.DownloadCallBack;
import trieu.mindvalley_trieu_android_test.lib.DownloadLibs;
import trieu.mindvalley_trieu_android_test.model.MyObj;
import trieu.mindvalley_trieu_android_test.view.fragments.MainFragmentInterface;

/**
 * Created by Apple on 10/1/16.
 */
public class MainController implements ControllerInterface<MainFragmentInterface> {

    private MainFragmentInterface mainFragment;

    @Override
    public void attactTo(MainFragmentInterface mainFragment) {
        this.mainFragment = mainFragment;
    }

    @Override
    public void detact() {
        this.mainFragment = null;
    }
    public void clearMemAndRequest(){
        CacheLib.getInstance().clearAll();
        request();
    }
    public void request(){
        mainFragment.showLoading();
        DownloadLibs.getJsonByUrl(mainFragment.getCContext(), "http://pastebin.com/raw/wgkJgazE", new DownloadCallBack<String>() {
            @Override
            public void onDownloadCompleted(String str) {
                Type listType = new TypeToken<ArrayList<MyObj>>(){}.getType();
                List<MyObj> list = new Gson().fromJson(str, listType);
                Log.e("", "");
                mainFragment.showContent(list);
            }

            @Override
            public void onDownloadError(Throwable t) {
                mainFragment.showError(t.getLocalizedMessage(), t, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        request();
                    }
                });
            }
        });
    }
}
